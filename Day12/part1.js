let fs = require('fs');
let input = 'input/input_part1.txt';
let cases = [];
let generations = [];
let genCount = 20;
let plantCount = 0;

function countPlants(gen) {
    let count = 0;
    for(let i = 0; i < gen.length; i++) {
        if(gen[i] === '#') {
            count++;
        }
    }

    return count;
}

function processPot(gen, curPot) {
    // determine pattern to match
    let pattern = '';
    let match = '.';
    pattern += gen[curPot - 2] === undefined ? '.' : gen[curPot - 2];
    pattern += gen[curPot - 1] === undefined ? '.' : gen[curPot - 1];
    pattern += gen[curPot];
    pattern += gen[curPot + 1] === undefined ? '.' : gen[curPot + 1];
    pattern += gen[curPot + 2] === undefined ? '.' : gen[curPot + 2];

    // Find a match and return the result
    for(let p = 0; p < cases.length; p++) {
        if(pattern === cases[p]['pattern']) {
            match = cases[p].result;
        }
    }

    return match;
}

console.time('execute');
let data = fs.readFileSync(input, 'utf8').toString().trim().split('\n');
let initialState = data[0].match(/(?<=initial state:)(.*)/)[0].trim().split('');

// Store the patterns and pattern results
for(let i = 2; i < data.length; i++) {
    let state = {
        pattern: data[i].substring(0, 5),
        result: data[i][data[i].length - 1]
    }
    cases.push(state);
}

generations.push(initialState);
let min = 0;
let max = generations[0].length;
let count = 0;
let prevCount = 0;

// Grow the generations of plants
for(let i = 0; i < genCount; i++) {
    prevCount = count;
    count = 0;
    generations.push([]);
    if(generations[i].slice(0, 5).join('') !== '.....') {
      generations[i] = ['.', '.'].concat(generations[i]);
      min = min - 2;
    } else {
      generations[i].splice(0, 3);
      min = min + 3;
    }
    if(generations[i].slice(generations[i].length - 6, generations[i].length - 1).join('') !== '.....') {
      generations[i].push(['.', '.']);
      max = max + 2;
    } else {
      generations[i].splice(generations[i].length - 3, generations[i].length - 1);
      max = max - 3;
    }

    for(let j = 0; j < generations[i].length; j++) {
        let char = processPot(generations[i], j);
        generations[i + 1].push(char);
    }

    for(let j = 0; j < Math.abs(min - max); j++) {
        if(generations[i][j] === '#') {
            count += (j + min);
        }
    }
}

// Perform the final count
for(let i = 0; i < Math.abs(min - max); i++) {
    if(generations[genCount][i] === '#') {
        plantCount += (i + min);
    }
}

console.log("Plant count: "+plantCount);
console.timeEnd('execute');

let fs = require('fs');
let input = 'input/input_part1.txt';
let polymer = '';
let pair0 = '';
let pair1 = '';

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  polymer = contents.toString().trim();

  // Iterate through the polymer until no reactive pairs remain
  for(var i = 0; i < polymer.length - 1; i++) {
      pair0 = polymer[i];
      pair1 = polymer[i + 1];
      let currentPair = pair0 + pair1;

      //First check to see if they are the same character
      if(pair0.toLowerCase() === pair1.toLowerCase()) {

          //Check the polarities now
          if(caseTest(pair0) !== caseTest(pair1)) {
              polymer = polymer.replace(currentPair, '');
              // move the index back to one position before the remove pair to look for the next pair.
              // Since we always iterate by one through the loop, we move it back 2, and have to check that
              // we don't end up at an index of -1.
              i = i - 2;
              if(i < -1) {
                  i = -1;
              }
          }
      }
  }

  console.log("Final remaining length: "+polymer.length);
  console.timeEnd('execute');

});

let caseTest = function(char) {
    if(char == char.toUpperCase()) {
        return 0;
    } else if (char == char.toLowerCase()) {
        return 1;
    }
}

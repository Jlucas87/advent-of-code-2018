let fs = require('fs');
let input = 'input/input_part1.txt';
let originalPolymer = '';
let polymer = '';
let pair0 = '';
let pair1 = '';
let alpha = 'abcdefghijklmnopqrstuvwxyz';
let polymerLengths = [];

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  originalPolymer = contents.toString().trim();

  // Iterate through the polymer until no reactive pairs remain
  for(let a = 0; a < alpha.length; a++) {
      // Create search pairs for each letter
      polymer = originalPolymer;
      var upper = new RegExp(alpha[a].toUpperCase(), 'g');
      var lower = new RegExp(alpha[a], 'g');
      let pair = alpha[a].toUpperCase() + alpha[a];
      polymer = polymer.replace(upper, '');
      polymer = polymer.replace(lower, '');
      polymer = polymer.split('');

      for(let i = 0; i < polymer.length - 1; i++) {
          pair0 = polymer[i];
          pair1 = polymer[i + 1];
          let currentPair = pair0 + pair1;

          //First check to see if they are the same character
          if(pair0.toLowerCase() === pair1.toLowerCase()) {

              //Check the polarities now
              if(caseTest(pair0) !== caseTest(pair1)) {
                  polymer.splice(i, 2);
                  // move the index back to one position before the remove pair to look for the next pair.
                  // Since we always iterate by one through the loop, we move it back 2, and have to check that
                  // we don't end up at an index of -1.
                  i = i - 2;
                  if(i < -1) {
                      i = -1;
                  }
              }
          }
      }
      let polyObj = {
          pair: pair,
          length: polymer.length
      };
      polymerLengths.push(polyObj);
      console.log('Polymer length with '+pair+' pairs removed: '+polymer.length);
  }

  // Finally, sort and display the polymer lengths
  polymerLengths.sort(lengthSort);
  console.log(polymerLengths);

  console.timeEnd('execute');

});

let caseTest = function(char) {
    if(char == char.toUpperCase()) {
        return 0;
    } else if (char == char.toLowerCase()) {
        return 1;
    }
}

let lengthSort = function(a, b) {
    if(a.length < b.length) {
        return -1;
    } else if(b.length < a.length) {
        return 1;
    }
    return 0;
}

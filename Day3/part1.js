var fs = require('fs');
var input = 'input/input_part1.txt';
var overlapSet = new Set();
var coordinates = new Set();
var claimArr = [];
var coords = '';
var dimensions = '';
var coordsArr = [];
var dimensionsArr = [];
var xMod = 0;
var yMod = 0;
var cx = 0;
var cy = 0;
var claimSize = 0;
var curCoord = "";

console.time('execute');

fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split('\n');

  for(var i = 0; i < resultArr.length - 1; i++) {
    claimArr = resultArr[i].split(' ');
    parseClaim(claimArr);

    for(var x = 0; x < cx; x++) {
      for(var y = 0; y < cy; y++) {
        curCoord = "" + (x + xMod) + "," + (y + yMod);
        if(coordinates.has(curCoord)) {
          overlapSet.add(curCoord);
        }
        coordinates.add(curCoord);
      }
    }
  }
  console.log(overlapSet.size);
  console.timeEnd('execute');
});

var parseClaim = function(claimArr) {
  coords = claimArr[2];
  dimensions = claimArr[3];
  coordsArr = coords.split(',');
  xMod = parseInt(coordsArr[0]);
  yMod = parseInt(coordsArr[1]);
  dimensionsArr = dimensions.split('x');
  cx = dimensionsArr[0];
  cy = dimensionsArr[1];
  claimSize = cx * cy;
}

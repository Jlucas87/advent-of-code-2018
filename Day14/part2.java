import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacob on 12/23/2018.
 */
public class part1 {

    protected static int elf1 = 3;
    protected static int e1p = 0;
    protected static int elf2 = 7;
    protected static int e2p = 1;
    // 702831
    protected static String input = "702831";
    protected static int length = 6;
    protected static ArrayList<Integer> recipes = new ArrayList<>();
    protected static long maxIterations = 500000000;
    protected static long iterations = 0;
    protected static long sum = 0;
    protected static List<Integer> test;

    public static void main(String [] args)
    {
        recipes.add(elf1);
        recipes.add(elf2);
        boolean doubleInc = false;
        while(iterations < maxIterations) {
            // Take the sum of the two recipes and append new scores to list
            sum = recipes.get(e1p) + recipes.get(e2p);
            String[] sumString = String.valueOf(sum).split("");
            if(sumString.length == 2) {
                doubleInc = true;
            } else {
                doubleInc = false;
            }
            for(int j = 0; j < sumString.length; j++) {
                recipes.add(Integer.valueOf(sumString[j]));
            }

            if(recipes.size() > length) {
                test = recipes.subList(recipes.size() - length, recipes.size());
                String testString = "";
                for(int z = 0; z < test.size(); z++) {
                    testString = testString.concat(String.valueOf(test.get(z)));
                }
                if(testString.equals(input)) {
                    System.out.println("We found a match after " + (recipes.size() - length) + " recipes");
                    break;
                }

                if(doubleInc == true) {
                    test = recipes.subList(recipes.size() - length - 1, recipes.size() - 1);
                    String testString2 = "";
                    for(int z = 0; z < test.size(); z++) {
                        testString2 = testString2.concat(String.valueOf(test.get(z)));
                    }

                    if(testString2.equals(input)) {
                        System.out.println("We found a match after " + (recipes.size() - length - 1) + " recipes");
                        break;
                    }
                }
            }

            // Move the elf recipes and reassign
            int newE1p = recipes.get(e1p) + 1 + e1p;
            if (newE1p > recipes.size() - 1) {
                newE1p = newE1p % (recipes.size());
            }
            e1p = newE1p;
            elf1 = recipes.get(e1p);

            int newE2p = recipes.get(e2p) + 1 + e2p;
            if (newE2p > recipes.size() - 1) {
                newE2p = newE2p % (recipes.size());
            }
            e2p = newE2p;
            elf2 = recipes.get(e2p);
            iterations++;
        }
    }
}

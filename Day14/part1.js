let elf1 = 3;
let e1p = 0;
let elf2 = 7;
let e2p = 1;
let input = 702831;
let recipes = [];

// Setup the initial list
console.time('execute');
recipes = new Array(elf1, elf2);
for(let i = 0; i < input + 10; i++) {
    // Take the sum of the two recipes and append new scores to list
    let sum = recipes[e1p] + recipes[e2p];
    sum = sum.toString().split('');
    for(let j = 0; j < sum.length; j++) {
        recipes.push(parseInt(sum[j]));
    }

    // Move the elf recipes and reassign
    let newE1p = recipes[e1p] + 1 + e1p;
    if (newE1p > recipes.length - 1) {
        newE1p = newE1p % (recipes.length);
    }
    e1p = newE1p;
    elf1 = recipes[e1p];

    let newE2p = recipes[e2p] + 1 + e2p;
    if (newE2p > recipes.length - 1) {
        newE2p = newE2p % (recipes.length);
    }
    e2p = newE2p;
    elf2 = recipes[e2p];
}

console.log(recipes.splice(input, 10).join(''));
console.timeEnd('execute');

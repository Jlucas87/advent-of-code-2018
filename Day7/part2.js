let fs = require('fs');
let input = 'input/input_part1.txt';
let split = '';
let dependencies = {};
let rules = new Set();
let rulesOrder = [];
let rulesAvailable = new Set();
let first = 1;
let second = 7;
let firstInstruction = '';
let rulesArray = null;
let next = '';
let worker1 = null;
let worker2 = null;
let worker3 = null;
let worker4 = null;
let worker5 = null;
let totalSeconds = 0;
let alphabet = [ 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' ];

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  let rulesList = contents.toString().trim().split('\n');

  // Split the sentences
  for(let i = 0; i < rulesList.length; i++) {
      let split = rulesList[i].split(' ');
      let a = split[first];
      let b = split[second];
      rules.add(a);
      rules.add(b);

      if(dependencies[b] === undefined) {
          dependencies[b] = new Set();
      }
      dependencies[b].add(a);
  }

  // Add any items that don't have dependencies to rules available
  for(let rule of rules) {
      if(dependencies[rule] === undefined) {
          rulesAvailable.add(rule);
      }
  }

  //Find the instruction order by selecting a rule during each iteration
  while(rules.size > 0) {
      // Find the next rule available from rules available in alpha order
      rulesArray = Array.from(rulesAvailable).sort();
      for(let i = 0; i < rulesArray.length; i++) {
          allocateToWorker(rulesArray[i]);
      }

      let completed = processWorkers();
      if(completed.length > 0) {
          for(let i = 0; i < completed.length; i++) {
              rules.delete(completed[i]);
              rulesOrder.push(completed[i]);

              // Add the newly ungated instructions to the available set
              processDependencies(completed[i]);
          }
      }

      totalSeconds++;
  }

  let finalOrder = "";
  for(let i = 0; i < rulesOrder.length; i++) {
      finalOrder += rulesOrder[i];
  }

  console.log(finalOrder);
  console.log(totalSeconds);
  console.timeEnd('execute');
});

// Update the dependency sets and remove empty sets from the object
let processDependencies = function(instruction) {
    for (let item in dependencies) {
        if(dependencies[item].has(instruction)) {
            dependencies[item].delete(instruction);
            if(dependencies[item].size === 0) {
                rulesAvailable.add(item);
                delete dependencies[item];
            }
        }
    }
}

let allocateToWorker = function(next) {
    if(worker1 === null) {
        worker1 = [next, alphabet.indexOf(next.toLowerCase()) + 1 + 60];
        rulesAvailable.delete(next);
    } else if(worker2 === null) {
        worker2 = [next, alphabet.indexOf(next.toLowerCase()) + 1 + 60];
        rulesAvailable.delete(next);
    } else if(worker3 === null) {
        worker3 = [next, alphabet.indexOf(next.toLowerCase()) + 1 + 60];
        rulesAvailable.delete(next);
    } else if(worker4 === null) {
        worker4 = [next, alphabet.indexOf(next.toLowerCase()) + 1 + 60];
        rulesAvailable.delete(next);
    } else if(worker5 === null) {
        worker5 = [next, alphabet.indexOf(next.toLowerCase()) + 1 + 60];
        rulesAvailable.delete(next);
    }
}

let processWorkers = function() {
    let completed = [];
    if(worker1 !== null) {
        worker1[1]--;
        if(worker1[1] === 0) {
            completed.push(worker1[0]);
            worker1 = null;
        }
    }
    if(worker2 !== null) {
        worker2[1]--;
        if(worker2[1] === 0) {
            completed.push(worker2[0]);
            worker2 = null;
        }
    }
    if(worker3 !== null) {
        worker3[1]--;
        if(worker3[1] === 0) {
            completed.push(worker3[0]);
            worker3 = null;
        }
    }
    if(worker4 !== null) {
        worker4[1]--;
        if(worker4[1] === 0) {
            completed.push(worker4[0]);
            worker4 = null;
        }
    }
    if(worker5 !== null) {
        worker5[1]--;
        if(worker5[1] === 0) {
            completed.push(worker5[0]);
            worker5 = null;
        }
    }

    return completed;
}

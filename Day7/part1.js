let fs = require('fs');
let input = 'input/input_part1.txt';
let split = '';
let dependencies = {};
let rules = new Set();
let rulesAvailable = new Set();
let rulesOrder = [];
let first = 1;
let second = 7;
let firstInstruction = '';
let rulesArray = null;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  let rulesList = contents.toString().trim().split('\n');

  // Split the sentences
  for(let i = 0; i < rulesList.length; i++) {
      let split = rulesList[i].split(' ');
      let a = split[first];
      let b = split[second];
      rules.add(a);
      rules.add(b);

      if(dependencies[b] === undefined) {
          dependencies[b] = new Set();
      }
      dependencies[b].add(a);
  }

  // Add any items that don't have dependencies to rules available
  for(let rule of rules) {
      if(dependencies[rule] === undefined) {
          rulesAvailable.add(rule);
      }
  }

  // Find the instruction order by selecting a rule during each iteration
  let next = '';
  while(rules.size > 0) {
      // Find the next rule available from rules available in alpha order
      rulesArray = Array.from(rulesAvailable).sort();
      let next = rulesArray[0];
      rulesOrder.push(next);
      rules.delete(next);
      rulesAvailable.delete(next);

      // Add the newly ungated instructions to the available set
      processDependencies(next);
  }

  let finalOrder = "";
  for(let i = 0; i < rulesOrder.length; i++) {
      finalOrder += rulesOrder[i];
  }
  console.log(finalOrder);
  console.timeEnd('execute');
});

// Update the dependency sets and remove empty sets from the object
let processDependencies = function(instruction) {
    for (let item in dependencies) {
        if(dependencies[item].has(instruction)) {
            dependencies[item].delete(instruction);
            if(dependencies[item].size === 0) {
                rulesAvailable.add(item);
                delete dependencies[item];
            }
        }
    }
}

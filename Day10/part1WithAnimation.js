let fs = require('fs');
let input = 'input/input_part1.txt';
let output = 'output/output.html';
let data;
let positions = {};
let pointMovement = 20;
let finalMinX = 0;
let script =
`<script>
function step(i) {
  if(i > 0) {
    document.getElementById(i - 1).classList.add("hidden");
  }
  var element = document.getElementById(i).classList.remove("hidden");
}
let i = 0;
function interval() {
  step(i)
  if (i < time_interval_total) {
    i++;
    setTimeout(interval, i == time_interval ? 1000 : 150);
  }
}
function animate() {
  setTimeout(interval, 0);
}
</script>`;
let style = '<style>div.hidden {display: none;}</style>';

// Function to move the points by their velocities
function movePoints(positions, direction) {
    for(let i = 0; i < positions.length; i++) {
        let x = parseInt(positions[i]['position'][0]);
        let y = parseInt(positions[i]['position'][1]);
        let xv = parseInt(positions[i]['velocity'][0]);
        let yv = parseInt(positions[i]['velocity'][1]);

        // Update the values
        if(direction === 0) {
            positions[i]['position'][0] = x + xv;
            positions[i]['position'][1] = y + yv;
        } else {
            positions[i]['position'][0] = x - xv;
            positions[i]['position'][1] = y - yv;
        }
    }

    return positions;
}

console.time('execute');
data = fs.readFileSync(input, 'utf8').toString().trim().split('\n');

// Create the positions and velocities object
for(let i = 0; i < data.length; i++){
    // Find the x and y coordinates
    let position = data[i].match(/(?<=position=<)(.*)(?=>\s)/)[0];
    position = position.split(', ');
    let x = parseInt(position[0].trim()) + 60000;
    let y = parseInt(position[1].trim()) + 60000;

    // Find the velocities of the points
    let velocity = data[i].match(/(?<=velocity=<)(.*)(?=>)/)[0];
    velocity = velocity.split(', ');
    let velX = velocity[0].trim();
    let velY = velocity[1].trim();

    // Add the data to the positions object
    positions[i] = {
        position: [x, y],
        velocity: [velX, velY]
    };
}

//  Find the smallest bounded area
let values = Object.values(positions);
let minBounds = 0;
for(let i = 0; i < 20000; i++) {
    let xMin = 100000;
    let xMax = 0;
    let yMin = 100000;
    let yMax = 0;
    for(let j = 0; j < values.length; j++) {
        let x = values[j]['position'][0];
        let y = values[j]['position'][1];
        if(x > xMax) {
            xMax = x;
        }
        if(x < xMin) {
            xMin = x;
        }
        if(y > yMax) {
            yMax = y;
        }
        if(y < yMin) {
            yMin = y;
        }
    }
    values = movePoints(values, 0);

    let bounds = (xMax - xMin) * (yMax - yMin);
    if(i === 0) {
        minBounds = bounds;
    } else {
        if(bounds < minBounds) {
            minBounds = bounds;
            finalMinX = xMin;
            finalMinY = yMin;
        } else {
            console.log("Time required to see word: "+(i - 1)+" seconds");
            i = 20000;
        }
    }
}

for(let i = 0; i < pointMovement; i++) {
    values = movePoints(values, 1);
}

let html = '<html>\n<head>\n' + style + '\n' + '</head>\n<body onload="animate();">\n' +
script.replace('time_interval_total', (pointMovement * 2) - 1).replace('time_interval', pointMovement - 1) + '\n';
let finalModifier = finalMinX;
if(finalMinY < finalModifier) {
    finalModifier = finalMinY;
}
for(let t = 0; t < pointMovement * 2; t++) {
    // Create a div for each time interval
    if(t !== 0) {
        html += '<div class="hidden" id="'+t+'">\n';
    } else {
        html += '<div id="'+t+'">\n';
    }
    for(let i = 0; i < values.length; i++) {
        let x = (values[i]['position'][0] - finalModifier + 200);
        let y = (values[i]['position'][1] - finalModifier + 200);
        let point = '<p style="position: fixed; top: '+y+'; left: '+(x)+';">.</p>\n';
        html += point;
    }
    html += '</div>\n';
    values = movePoints(values, 0);
}
html += '</body>\n</html>';

fs.writeFileSync(output, html, function(err, data){
    if (err) {
        console.log(err);
    }
    console.log("Successfully Written to File.");
});

console.timeEnd('execute');

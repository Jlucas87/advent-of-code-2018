let fs = require('fs');
let input = 'input/input_part1.txt';
let output = 'output/output.html';
let data;
let positions = {};
let finalMinX = 0;
let finalMinY = 0;

// Function to move the points by their velocities
function movePoints(positions) {
    for(let i = 0; i < positions.length; i++) {
        let x = parseInt(positions[i]['position'][0]);
        let y = parseInt(positions[i]['position'][1]);
        let xv = parseInt(positions[i]['velocity'][0]);
        let yv = parseInt(positions[i]['velocity'][1]);

        // Update the values
        positions[i]['position'][0] = x + xv;
        positions[i]['position'][1] = y + yv;
    }

    return positions;
}

console.time('execute');
data = fs.readFileSync(input, 'utf8').toString().trim().split('\n');

// Create the positions and velocities object
for(let i = 0; i < data.length; i++){
    // Find the x and y coordinates
    let position = data[i].match(/(?<=position=<)(.*)(?=>\s)/)[0];
    position = position.split(', ');
    let x = parseInt(position[0].trim()) + 60000;
    let y = parseInt(position[1].trim()) + 60000;

    // Find the velocities of the points
    let velocity = data[i].match(/(?<=velocity=<)(.*)(?=>)/)[0];
    velocity = velocity.split(', ');
    let velX = velocity[0].trim();
    let velY = velocity[1].trim();

    // Add the data to the positions object
    positions[i] = {
        position: [x, y],
        velocity: [velX, velY]
    };
}

//  Find the smallest bounded area
let values = Object.values(positions);
let minBounds = 0;
for(let i = 0; i < 20000; i++) {
    let xMin = 100000;
    let xMax = 0;
    let yMin = 100000;
    let yMax = 0;
    for(let j = 0; j < values.length; j++) {
        let x = values[j]['position'][0];
        let y = values[j]['position'][1];
        if(x > xMax) {
            xMax = x;
        }
        if(x < xMin) {
            xMin = x;
        }
        if(y > yMax) {
            yMax = y;
        }
        if(y < yMin) {
            yMin = y;
        }
    }

    let bounds = (xMax - xMin) * (yMax - yMin);
    if(i === 0) {
        minBounds = bounds;
    } else {
        if(bounds < minBounds) {
            minBounds = bounds;
            finalMinX = xMin;
            finalMinY = yMin;
        } else {
            break;
        }
    }

    values = movePoints(values);
}

// Output the final set of points to html
let html = '<html>\n<head></head>\n<body>\n';
let finalModifier = finalMinX;
if(finalMinY < finalModifier) {
    finalModifier = finalMinY;
}
for(let i = 0; i < values.length; i++) {
    let x = (values[i]['position'][0] - finalModifier + 10);
    let y = (values[i]['position'][1] - finalModifier + 10);
    let point = '<p style="position: fixed; top: '+y+'; left: '+x+';">.</p>\n';
    html += point;
}
html += '</body>\n</html>';

fs.writeFileSync(output, html, function(err, data){
    if (err) {
        console.log(err);
    }
    console.log("Successfully Written to File.");
});

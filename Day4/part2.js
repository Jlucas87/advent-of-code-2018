let fs = require('fs');
let input = 'input/input_part1.txt';
let guardsArray = [];
let guardSleepTime = {};
let guardSeconds = {};
let start = 0;
let end = 0;
let guardNum = 0;
let maxGuard = 0;
let maxMinuteVal = 0;
let maxMinute = 0;
// Checks
const SHIFT = 'begins shift';
const SLEEP = 'falls asleep';
const WAKE = 'wakes up';

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split('\n');

  // Sort the data by date and time and separate key pieces of data
  for(var i = 0; i < resultArr.length - 1; i++) {
    var claimArr = resultArr[i].split('] ');
    claimArr[0] = claimArr[0].replace('[', '');
    var sortKey = claimArr[0].replace(/-/g, '').replace(' ', '').replace(':', '');
    claimArr.push(sortKey);
    guardsArray.push(claimArr);
  }

  // Sort the array of arrays using a comparator
  guardsArray.sort(Comparator);

  // Count up the minutes each guard spends asleep
  for(i = 0; i < guardsArray.length; i++) {
    let action = guardsArray[i][1];
    let minute = guardsArray[i][0].split(' ')[1].split(':')[1];

    if(action.indexOf(SHIFT) > -1) {
      guardNum = action.split(' ')[1].replace('#', '');
      start = 0;
      end = 0;
    } else if(action.indexOf(SLEEP) > -1) {
      start = parseInt(minute);
    } else if(action.indexOf(WAKE) > -1) {
      // Add the guard total seconds asleep
      end = parseInt(minute);
      if(guardSeconds[guardNum] === undefined) {
        guardSeconds[guardNum] = {};
      }

      // Add the guard's minute counts to find the minute asleep the most often
      for(var j = start; j < end; j++) {
        if(guardSeconds[guardNum][j] === undefined) {
          guardSeconds[guardNum][j] = 0;
        }
        guardSeconds[guardNum][j]++;
      }
    }
  }

  // Find the most popular minute
  let guardSecondsKeys = Object.keys(guardSeconds);
  let guardSecondsValues = Object.values(guardSeconds);
  for(i = 0; i < guardSecondsKeys.length; i++) {
    guardNum = guardSecondsKeys[i];
    let guardValues = guardSecondsValues[i];
    let guardValueKeys = Object.keys(guardValues);
    let guardValueValues = Object.values(guardValues);

    for(var j = 0; j < guardValueValues.length; j++) {
      if(guardValueValues[j] > maxMinuteVal) {
        maxMinuteVal = guardValueValues[j];
        maxMinute = guardValueKeys[j];
        maxGuard = guardNum;
      }
    }
  }

  console.log("Max minute: "+maxMinute);
  console.log("Max minute repeats: "+maxMinuteVal);
  console.log("Max guard id: "+ maxGuard);
  console.timeEnd('execute');
});

function Comparator(a, b) {
  if (a[2] < b[2]) return -1;
  if (a[2] > b[2]) return 1;
  return 0;
}

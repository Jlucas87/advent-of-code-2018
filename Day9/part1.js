let fs = require('fs');
let input = 'input/input_part1.txt';
let players = 0;
let lastMarble = 0;
let marbles = [];
let currentMarble = 0;
let playerScores = {};
let currentPlayer = 1;
const pointMarble = 23;
const Deque = require('double-ended-queue');

function leftShift(marbles, times) {
  for(let i = 0;  i < times; i++) {
      marbles.push(marbles.shift());
  }

  return marbles;
}

function rightShift(marbles, times) {
  for(let i = 0;  i < times; i++) {
      marbles.unshift(marbles.pop());
  }

  return marbles;
}

let data;
console.time('execute');
data = fs.readFileSync(input, 'utf8').toString().trim().split(' ');

players = data[0];
lastMarble = data[6];

// Generate players object based on the number of players
for(let i = 1; i <= players; i++) {
  playerScores[i] = 0;
}

// Push the first three marbles
marbles = new Deque([0, 2, 1]);
currentMarble = 3;
currentPlayer = 3;

// Create the marble list and begin allocating points to players
while(currentMarble <= lastMarble) {
    // Remove the current marble and the marble 7 back as well
    if(currentMarble % pointMarble == 0) {
        let score = currentMarble;
        marbles = rightShift(marbles, 7);
        score += marbles.pop();
        if(!playerScores[currentPlayer] === undefined) {
            playerScores[currentPlayer] = 0;
        }
        playerScores[currentPlayer] = playerScores[currentPlayer] + score;
        marbles = leftShift(marbles, 1);

    // Shift the deque and add the marble to it
    } else {
        marbles = leftShift(marbles, 1);
        marbles.push(currentMarble);
    }

    if(currentPlayer == players) {
        currentPlayer = 1;
    } else {
        currentPlayer++;
    }

    currentMarble++;
}

// Find the highest player score
let values = Object.values(playerScores);
let maxMarble = 0;
let maxPlayer = 0;
for(let i = 0; i < values.length; i++) {
    if(values[i] > maxMarble) {
        maxMarble = values[i];
        maxPlayer = i;
    }
}

console.log("Player "+maxPlayer+" had the highest score with a grand total of: "+maxMarble);
console.timeEnd('execute');

let input = 7803;
let fuelCells = {};
let maxPower = {
    coordinate: '',
    power: 0,
    size: 0
};

console.time('execute');

// Find the power output of each fuel cell based on the x and y coordinates
function calculatePower(x, y) {
    let rackId = x + 10;
    let power = rackId * y;
    power += input;
    power = power * rackId;
    power = power.toString();
    let powerArr = power.split('');
    power = parseInt(powerArr[powerArr.length - 3]) - 5;

    return power;
}

// Calculate the total power of the 3 x 3 grid
function calculateGridPower(x, y, size, fuelCells, interval) {
    let totalPower = 0;
    for (let i = 0; i < size; i += interval) {
        for (let j = 0; j < size; j += interval) {
            totalPower += fuelCells[x+i][y+j];
        }
    }

    return totalPower;
}

// Calculate the total power of the 3 x 3 grid
function calculateGridPowerEdge(x, y, size) {
    let totalPower = 0;
    for(let i = 0; i < size; i++) {
        totalPower += fuelCells[1][x + i][y + size - 1];
    }
    for(let i = 0; i < size - 1; i++) {
        totalPower += fuelCells[1][x + size - 1][y + i];
    }

    return totalPower;
}

// Create the fuel cell power chart for the size 1 case
fuelCells[1] = {};
for (let x = 1; x <= 300; x++) {
    // Create new row for the fuel cells
    fuelCells[1][x] = {};

    for (let y = 1; y <= 300; y++) {
        //Calculate the power of the current fuel cell
        fuelCells[1][x][y] = calculatePower(x, y);
    }
}

// Find the section with the greatest total power
for(let size = 2; size <= 300; size++) {
    fuelCells[size] = {};
    // Iterate through the smaller set
    if(size % 2 === 0) {
        let fullGrid = size / 2;
        for (let x = 1; x <= 300 - size + 1; x++) {
            fuelCells[size][x] = {};

            for (let y = 1; y <= 300 - size + 1; y++) {
                let gridPower = calculateGridPower(x, y, size, fuelCells[fullGrid], fullGrid);
                fuelCells[size][x][y] = gridPower;
                if (gridPower > maxPower['power']) {
                    maxPower['power'] = gridPower;
                    maxPower['coordinate'] = x + ',' + y;
                    maxPower['size'] = size;
                }
            }
        }
    } else if(size % 3 === 0) {
        let fullGrid = size / 3;
        for (let x = 1; x <= 300 - size + 1; x++) {
            fuelCells[size][x] = {};

            for (let y = 1; y <= 300 - size + 1; y++) {
                let gridPower = calculateGridPower(x, y, size, fuelCells[fullGrid], fullGrid);
                fuelCells[size][x][y] = gridPower;
                if (gridPower > maxPower['power']) {
                    maxPower['power'] = gridPower;
                    maxPower['coordinate'] = x + ',' + y;
                    maxPower['size'] = size;
                }
            }
        }
    } else {
        for (let x = 1; x <= 300 - size + 1; x++) {
            fuelCells[size][x] = {};

            for (let y = 1; y <= 300 - size + 1; y++) {
                let gridPower = fuelCells[size - 1][x][y];
                gridPower += calculateGridPowerEdge(x, y, size);
                fuelCells[size][x][y] = gridPower;
                if (gridPower > maxPower['power']) {
                    maxPower['power'] = gridPower;
                    maxPower['coordinate'] = x + ',' + y;
                    maxPower['size'] = size;
                }
            }
        }
    }
}

console.log('Most powerful grid has been located at: '+ maxPower['coordinate'] + ' with a size of '+maxPower['size']+' and '+maxPower['power'] + ' total power!');
console.timeEnd('execute');

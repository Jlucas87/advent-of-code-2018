let input = 7803;
let fuelCells = {};
let maxPower = {
    coordinate: null,
    power: 0
}

console.time('execute');

// Find the power output of each fuel cell based on the x and y coordinates
function calculatePower(x, y) {
    let rackId = x + 10;
    let power = rackId * y;
    power += input;
    power = power * rackId;
    power = power.toString();
    let powerArr = power.split('');
    power = parseInt(powerArr[powerArr.length - 3]) - 5;

    return power;
}

// Calculate the total power of the 3 x 3 grid
function calculateGridPower(x, y) {
    let totalPower = 0;
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            totalPower += fuelCells[x+i][y+j];
        }
    }

    return totalPower;
}

// Create the fuel cell power chart
for (let x = 1; x <= 300; x++) {
    // Create new row for the fuel cells
    fuelCells[x] = {};

    for (let y = 1; y <= 300; y++) {
        //Calculate the power of the current fuel cell
        fuelCells[x][y] = calculatePower(x, y);
    }
}

// Find the section with the greatest total power
for (let x = 1; x <= 298; x++) {

    for (let y = 1; y <= 298; y++) {
        let gridPower = calculateGridPower(x, y);
        if (gridPower > maxPower['power']) {
            maxPower['power'] = gridPower;
            maxPower['coordinate'] = x + ',' + y;
        }
    }
}

console.log('Most powerful grid has been located at: '+ maxPower['coordinate'] + ' with '+maxPower['power'] + ' total power!');
console.timeEnd('execute');

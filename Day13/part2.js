let fs = require('fs');
let input = 'input/input_part1.txt';
let carts = [];
let cartShapes = ['<', '>', '^', 'v'];
let cartNum = 0;
let turnPattern = ['L', 'S', 'R'];
let crashState = false;
let movementMap = {
  '>': '1,y',
  '<': '-1,y',
  '^': '-1,x',
  'v': '1,x'
};
let length;

function compare(a,b) {
  let aComp = (a.x * length) + (a.y);
  let bComp = (b.x * length) + (b.y);

  if (aComp < bComp)
    return -1;
  if (aComp > bComp)
    return 1;
  return 0;
}

function processMovement(cart) {
    // Find the next move for the cart
    let symbol = cart['direction'];
    let track = cart['track'];
    let turn = turnPattern[cart['turn']];
    let x = cart['x'];
    let y = cart['y'];
    let m = movementMap[symbol].split(',');

    // Return the track back to its old symbol
    tracks[x][y] = cart['track'];

    // Determine movement
    if(track === '-' || track === '|') {
        if(m[1] === 'x') {
            cart['x'] = x + parseInt(m[0]);
        } else {
            cart['y'] = y + parseInt(m[0]);
        }
    } else if (track === '/') {
        if(symbol === '<') {
            cart['direction'] = 'v';
            cart['x'] = x + 1;
        } else if (symbol === '>') {
            cart['direction'] = '^';
            cart['x'] = x - 1;
        } else if (symbol === '^') {
            cart['direction'] = '>';
            cart['y'] = y + 1;
        } else {
            cart['direction'] = '<';
            cart['y'] = y - 1;
        }
    } else if (track === '\\') {
        if(symbol === '<') {
            cart['direction'] = '^';
            cart['x'] = x - 1;
        } else if (symbol === '>') {
            cart['direction'] = 'v';
            cart['x'] = x + 1;
        } else if (symbol === '^') {
            cart['direction'] = '<';
            cart['y'] = y - 1;
        } else {
            cart['direction'] = '>';
            cart['y'] = y + 1;
        }
    } else if (track === '+') {
        if(turn === 'S') {
            cart['turn'] = 2;
            if(symbol === '>') {
                cart['y'] = y + 1;
            } else if(symbol === '<') {
                cart['y'] = y - 1;
            } else if(symbol === '^') {
                cart['x'] = x - 1;
            } else {
                cart['x'] = x + 1
            }
        } else if(turn === 'L') {
            cart['turn'] = 1;
            if(symbol === '>') {
                cart['x'] = x - 1;
                cart['direction'] = '^';
            } else if(symbol === '<') {
                cart['x'] = x + 1;
                cart['direction'] = 'v';
            } else if(symbol === '^') {
                cart['y'] = y - 1;
                cart['direction'] = '<';
            } else {
                cart['y'] = y + 1;
                cart['direction'] = '>';
            }
        } else if(turn === 'R') {
            cart['turn'] = 0;
            if(symbol === '>') {
                cart['x'] = x + 1;
                cart['direction'] = 'v';
            } else if(symbol === '<') {
                cart['x'] = x - 1;
                cart['direction'] = '^';
            } else if(symbol === '^') {
                cart['y'] = y + 1;
                cart['direction'] = '>';
            } else {
                cart['y'] = y - 1;
                cart['direction'] = '<';
            }
        }
    }

    // Update the track based on the movement
    cart['track'] = tracks[cart['x']][cart['y']];
    return cart;
}

function collisionCheck(carts) {
    let collision = null;
    for(let i = 0; i < carts.length - 1; i++) {
        for(let j = i + 1; j < carts.length; j++) {
            let c1 = carts[i];
            let c2 = carts[j];
            if(c1['x'] === c2['x'] && c1['y'] === c2['y']) {
                // Return the track to its original state before returning
                tracks[c1['x']][c1['y']] = c1['track'];

                return [i, j];
            }
        }
    }

    return collision;
}

console.time('execute');
var tracks = fs.readFileSync(input, 'utf8').toString().split('\n');
tracks.splice(-1);
length = tracks[0].length;

// Find the initial states of the carts
for(let i = 0; i < tracks.length; i++) {
    tracks[i] = tracks[i].split('');
    for(let j = 0; j < tracks[i].length; j++) {
        if(cartShapes.includes(tracks[i][j])) {
            let track = ('>' == tracks[i][j] || '<' == tracks[i][j]) ? '-' : '|';
            carts.push({
                x: i,
                y: j,
                direction: tracks[i][j],
                turn: 0,
                track
            });

            // Set the track to its starting symbol
            tracks[i][j] = track;
        }
    }
}

// Initiate and continue cart movement until a crash is detected
let oneRemains = false
let prevCart = null;
while(!oneRemains) {
  carts.sort(compare);
  for(let i = 0; i < carts.length; i++) {
      // Move each cart in turn
      carts[i] = processMovement(carts[i]);
      collide = collisionCheck(carts);
      if(collide !== null) {
          carts.splice(collide[0], 1);
          carts.splice(collide[1] - 1, 1);
          if(carts.length === 1) {
              // Process the last tick and end
              oneRemains = true;
          } else {
               i--;
          }
      }
  }
}

console.log(carts[0]);
console.timeEnd('execute');

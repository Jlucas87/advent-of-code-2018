var fs = require('fs');
var input = 'input/part1_input.txt';
var frequencySum = 0;
var freqSetSize = 0;
const freqSet = new Set();

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split("\n");
  var matchFound = false;
  while(!matchFound) {
    for(var i = 0; i < resultArr.length - 1; i++) {
      if(resultArr[i].charAt(0) === '+') {
        frequencySum = frequencySum + parseInt(resultArr[i].substring(1));
      } else {
        frequencySum = frequencySum - parseInt(resultArr[i].substring(1));
      }

      if(freqSet.has(frequencySum)) {
        console.log("Found something!");
        console.log("repeated frequency: "+frequencySum);
        matchFound = true;
        i = resultArr.length;
      }

      freqSet.add(frequencySum);
    }
  }

  console.timeEnd('execute');
});

var fs = require('fs');
var input = 'input/part1_input.txt';
var frequencySum = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split("\n");

  frequencySum = sumFrequencies(frequencySum, resultArr);

  console.log(frequencySum);
  console.timeEnd('execute');
});

var sumFrequencies = (frequencySum, list) => {
  var head = list.shift();
  if(head.charAt(0) === '+') {
    frequencySum += parseInt(head.substring(1));
  } else {
    frequencySum -= parseInt(head.substring(1));
  }

  if(list.length > 1) {
    return sumFrequencies(frequencySum, list);
  } else {
    return frequencySum;
  }
};

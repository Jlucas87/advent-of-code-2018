let fs = require('fs');
let input = 'input/input_part1.txt';
let index = 0;
let metaSum = 0;
let nodes = null;
let rootSum = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  nodes = contents.toString().trim().split(' ');

  let finalResult = findMetaData([index, metaSum]);

  console.log("We are on index: "+ finalResult[0]);
  console.log("The metadata total is: " + finalResult[1]);
  console.timeEnd('execute');
});

let findMetaData = function(meta) {
    let children = parseInt(nodes[meta[0]]);
    let metadata = parseInt(nodes[meta[0] + 1]);

    //Are we at the bottom of a branch?
    if(children === 0) {
        index = index + 2;
        for(let i = 0; i < metadata; i++) {
            metaSum += parseInt(nodes[index]);
            index++;
        }

        return [index, metaSum];
    } else {
        index = index + 2;
        // Find the metadata for all children
        let childrenResults = [];
        for(let i = 0; i < children; i++) {
            metaSum = 0;
            let result = findMetaData([index, metaSum]);
            index = result[0];
            metaSum = result[1];
            childrenResults.push(metaSum);
        }

        // Find the data for the current level
        metaSum = 0;
        for(let i = 0; i < metadata; i++) {
            let metaIndex = parseInt(nodes[index]);
            if(metaIndex <= childrenResults.length) {
                metaSum += childrenResults[metaIndex - 1];
            }
            index++
        }

        return [index, metaSum];
    }
}

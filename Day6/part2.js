let fs = require('fs');
let input = 'input/input_part1.txt';
let xMax = 0;
let xMin = 9999;
let yMax = 0;
let yMin = 9999;
let coords = [];
let coordsInArea = [];
let distances = [];
let x = 0;
let y = 0;
const MAX = 10000;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  let points = contents.toString().trim().split('\n');

  // Firstly, determine the extreme point bounds. We won't measure
  // distances outside of these bounds.
  for(let i = 0; i < points.length; i++) {
      point = points[i].split(',');
      let x = parseInt(point[0]);
      let y = parseInt(point[1]);
      if(x > xMax) {
          xMax = x;
      }
      if(x < xMin) {
          xMin = x;
      }
      if(y > yMax) {
          yMax = y;
      }
      if(y < yMin) {
          yMin = y;
      }

      let coord = [x, y];
      coords.push(coord);
  }

  // Find the closest point within the bound area
  for(let x = xMin; x < xMax; x++) {
      for(let y = yMin; y < yMax; y++) {
          distances = [];

          for(i = 0; i < coords.length; i++) {
              let dist = mDist(x, y, coords[i][0], coords[i][1]);
              distances.push(dist);
          }

          // Find sum of distances
          let sum = 0;
          for(i = 0; i < distances.length; i++) {
              sum += distances[i];
          }

          if(sum < 10000) {
              mainAreaCoord = [x, y];
              coordsInArea.push(mainAreaCoord);
          }
      }
  }

  console.log('Size of total area: ' + (xMax - xMin) * (yMax - yMin));
  console.log("Size of Area: " + coordsInArea.length);
  console.timeEnd('execute');
});

// Find manhattan distance between two points
let mDist = function(x1, y1, x2, y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

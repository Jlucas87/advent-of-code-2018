let fs = require('fs');
let input = 'input/input_part1.txt';
let xMax = 0;
let xMin = 9999;
let yMax = 0;
let yMin = 9999;
let coords = [];
let infiniteCoords = [];
let coordDistances = {};
let closestCoord = null;
let coordFinalCounts = {};
let x = 0;
let y = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  let points = contents.toString().trim().split('\n');

  // Firstly, determine the extreme point bounds. We won't measure
  // distances outside of these bounds.
  for(let i = 0; i < points.length; i++) {
      point = points[i].split(',');
      let x = parseInt(point[0]);
      let y = parseInt(point[1]);
      if(x > xMax) {
          xMax = x;
      }
      if(x < xMin) {
          xMin = x;
      }
      if(y > yMax) {
          yMax = y;
      }
      if(y < yMin) {
          yMin = y;
      }

      let coord = [x, y];
      coords.push(coord);
  }

  // Infinity Checker
  for(i = 0; i < coords.length; i++) {
      let x = coords[i][0];
      let y = coords[i][1];

      if(infinityDetector(x, y, coords)) {
          infiniteCoords.push(i);
      };
  }

  // Find the closest point within the bound area
  let distances = [];
  for(let x = xMin; x < xMax; x++) {
      for(let y = yMin; y < yMax; y++) {
          closestCoord = 0;
          let minDist = 99999;
          distances = [];

          for(i = 0; i < coords.length; i++) {
              let dist = mDist(x, y, coords[i][0], coords[i][1]);
              distances.push(parseInt(dist));
              if(dist < minDist) {
                  minDist = dist;
                  closestCoord = i;
              }
          }
          distances.sort(sortInt);

          // Add the closest coordinate to the main tracking object
          if(distances[0] !== distances[1]) {
              coordDistances[''+x+','+y] = closestCoord;
          } else {
              coordDistances[''+x+','+y] =  '.';
          }
      }
  }

  //Find the final counts
  let cdValues = Object.values(coordDistances);
  for(i = 0; i < cdValues.length; i++) {
      let val = cdValues[i];
      if(coordFinalCounts[val] === undefined) {
          coordFinalCounts[val] = 1;
      } else {
          coordFinalCounts[val]++;
      }
  }

  // Remove infinite areas based on earlier test
  let largestValue = 0;
  for(i = 0; i < infiniteCoords.length; i++) {
      delete coordFinalCounts[infiniteCoords[i]];
  }

  cdValues = Object.values(coordFinalCounts);
  for(i = 0; i < cdValues.length; i++) {
      if(cdValues[i] > largestValue) {
          largestValue = cdValues[i];
      }
  }

  console.log(coordFinalCounts);
  console.log("Largest final Values: "+largestValue);
  console.timeEnd('execute');
});

// Find manhattan distance between two points
let mDist = function(x1, y1, x2, y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

let sortInt = function sortNumber(a,b) {
    return a - b;
}

/**
* Test points in four cardinal directions outside of the bound area defined by
* the most extreme points in each direction. The bound area is the rectangle on the grid
* utilizing the 4 most extreme points. Top left, top right, bottom left, and bottom right.
*/
let infinityDetector = function(x, y, testCoords) {
    let northCoord = [x, y+9999];
    let southCoord = [x, y-9999];
    let eastCoord = [x+9999, y];
    let westCoord = [x-9999, y];

    // Compare test points with the input point
    let nCoord = mDist(x, y, northCoord[0], northCoord[1]);
    let sCoord = mDist(x, y, southCoord[0], southCoord[1]);
    let eCoord = mDist(x, y, eastCoord[0], eastCoord[1]);
    let wCoord = mDist(x, y, westCoord[0], westCoord[1]);

    for(let i = 0; i < testCoords.length; i++) {
        // If it's the same coordinate, skip!
        if(testCoords[i][0] !== x && testCoords[i][1] !== y) {
            let ndist = mDist(testCoords[i][0], testCoords[i][1], northCoord[0], northCoord[1]);
            if(ndist < nCoord) {
                nCoord = ndist;
            }
            let sdist = mDist(testCoords[i][0], testCoords[i][1], southCoord[0], southCoord[1]);
            if(sdist < sCoord) {
                sCoord = sdist;
            }
            let edist = mDist(testCoords[i][0], testCoords[i][1], eastCoord[0], eastCoord[1]);
            if(edist < eCoord) {
                eCoord = edist;
            }
            let wdist = mDist(testCoords[i][0], testCoords[i][1], westCoord[0], westCoord[1]);
            if(wdist < wCoord) {
                wCoord = wdist;
            }
        }
    }

    if(sCoord === 9999 || nCoord === 9999 || eCoord === 9999 || wCoord === 9999) {
        return true;
    }

    return false;
}

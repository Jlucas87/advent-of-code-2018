var fs = require('fs');
var input = 'input/part1_input.txt';
var diff = 0;
var matches = "";

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split("\n");

  for(var i = 0; i < resultArr.length - 1; i++) {
    var string = resultArr[i];

    for(var j = i; j < resultArr.length - 1; j++) {
      var compareString = resultArr[j];
      matches = "";
      diff = 0;

      for(var k = 0; k < compareString.length; k++) {
        if(string.charAt(k) === compareString.charAt(k)) {
          matches += string.charAt(k);
        } else {
          diff++;
        }
      }

      if(diff === 1) {
        j = resultArr.length;
        i = resultArr.length;
      }
    }
  }
  
  console.log(matches);
  console.timeEnd('execute');
});

var fs = require('fs');
var input = 'input/part1_input.txt';
var totalTwos = 0;
var totalThrees = 0;
var characterCount = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  var resultArr = contents.toString().split("\n");

  for(var i = 0; i < resultArr.length - 1; i++) {
    var string = resultArr[i];
    characterCount = {};

    for(var j = 0; j < string.length; j++) {
      var char = string.charAt(j);
      if(char in characterCount) {
        characterCount[char] += 1;
      } else {
        characterCount[char] = 1;
      }
    }

    var values = Object.values(characterCount);
    if (values.indexOf(2) > -1) {
      totalTwos++;
    }
    if (values.indexOf(3) > -1) {
      totalThrees++;
    }
  }

  var checkSum = totalTwos * totalThrees;
  console.log(checkSum);
  console.timeEnd('execute');
});
